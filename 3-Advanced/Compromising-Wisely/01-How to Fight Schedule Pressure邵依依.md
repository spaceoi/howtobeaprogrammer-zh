# How to Fight Schedule Pressure
# 如何抵抗进度压力

Time-to-market pressure is the pressure to deliver a good product quickly. It is good because it reflects a financial reality, and is healthy up to a point. Schedule pressure is the pressure to deliver something faster than it can be delivered and it is wasteful, unhealthy, and all too common.

上市时机压力是尽快发布好的产品的压力。由于能够折射出财务实情，上市时机压力是有益处的，并在一定程度上是健康的。日程压力是一种快于实际能力发布产品的压力，日程压力是浪费的、不健康的，却又普遍存在。

Schedule pressure exists for several reasons. The people who task programmers do not fully appreciate what a strong work ethic we have and how much fun it is to be a programmer. Perhaps because they project their own behaviour onto us, they believe that asking for it sooner will make us work harder to get it there sooner. This is probably actually true, but the effect is very small, and the damage is very great. Additionally, they have no visibility into what it really takes to produce software. Not being able to see it, and not be able to create it themselves, the only thing they can do is see time-to-market pressure and fuss at programmers about it.

日程压力的存在基于几个原因。给程序员分配任务的人，并不能完全意识到我们的职业道德准则、以及作为程序员有多么快乐。也许是因为他们把自身的行为投射给了我们，他们相信催促我们努力工作能更快得到结果。也许事实如此，但其效果微乎其微，造成的损失会很大。另外，他们并不了解到底如何开发软件。既不了解，也不懂得开发，他们能做的事情仅仅是利用日程压力来烦扰程序员们。

The key to fighting schedule pressure is simply to turn it into time-to-market pressure. The way to do this to give visibility into the relationship between the available labour and the product. Producing an honest, detailed, and most of all, understandable estimate of all the labour involved is the best way to do this. It has the added advantage of allowing good management decisions to be made about possible functionality trade-offs.

抗争日程压力的关键就是简单地把它转化成上市时机压力。这种转化能够明晰可进行的工作与产品之间的关系。提出一个坦诚的、详尽的，以及更重要地，可理解的对所有工作的估算，是进行转化的最好的方法。估算的附加优点是促使对可实现的功能权衡做出好的决策。

The key insight that the estimate must make plain is that labour is an almost incompressible fluid. You can't pack more into a span of time anymore than you can pack more water into a container over and above that container's volume. In a sense, a programmer should never say ‘no’, but rather to say ‘What will you give up to get that thing you want?’ The effect of producing clear estimates will be to increase the respect for programmers. This is how other professionals behave. Programmers' hard work will be visible. Setting an unrealistic schedule will also be painfully obvious to everyone. Programmers cannot be hoodwinked. It is disrespectful and demoralizing to ask them to do something unrealistic. Extreme Programming amplifies this and builds a process around it; I hope that every reader will be lucky enough to use it.

由于工作就像几乎不可压缩的流体，估算也必须做的简单。时间无法压缩，就像你不能把超过容器体积的水装在容器里一样。在某种意义上，一个程序员应该尽量表达“你愿意放弃什么去获得你想要的？”，不应该说“不”。提出清晰估算是对程序员更好的尊敬形式。其他职业人士正是这样做的。程序员的努力工作是可见的。设置一个不切实际的时刻表对每个人也是明显的痛苦。不要欺骗程序员。要求他们做不现实的事情是无礼且令人泄气的。极限编程放大了估算的作用并围绕其设计了一套流程，我希望每一个读者有幸能够运用到极限编程。

下一篇 [如何理解用户](02-How_to_Understand_the_User.md)
